<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    function register()
    {
        // return view('register');
        $output = 'Form Input Sederhana';
        return view('register', array(
            'content' => $output
        ));
    }
    function welcome(Request $request)
    {
        $nama = $request->firstname;
        $namab = $request->lastname;
        return view('welcome', compact('nama', 'namab'));
    }
}
