<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Day1 Amanda</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>

  <body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form method="POST" action="/welcome">
        {{csrf_field()}}
      First Name:<br /><br />
      <input type="text" name="firstname" value="" placeholder="First Name" />
      <br /><br />
      Last Name:<br /><br />
      <input type="text" name="lastname" value="" placeholder="Last Name" />
      <br /><br />

      Gender: <br /><br />
      <input type="radio" name="Gender" /> Male <br />
      <input type="radio" name="Gender" /> Female <br />
      <input type="radio" name="Gender" /> Other <br /><br />

      Nationality: <br /><br />
      <select name="" id="">
        <option value="">Indonesia</option>
        <option value="">Singapore</option>
        <option value="">Malaysia</option>
        <option value="">Australia</option></select
      ><br /><br />

      Language Spoken: <br /><br />
      <input type="checkbox" name="Language Spoken" /> Bahasa Indonesia <br />
      <input type="checkbox" name="Language Spoken" /> English <br />
      <input type="checkbox" name="Language Spoken" /> Other <br /><br />

      Bio: <br /><br />
      <textarea name="" id="" cols="30" rows="10"></textarea><br />
      <button type="submit">Sign Up</button>
    </form>

  </body>
</html>
